
#include <vector>
#include <list>
#include <queue>
#include <map>
#include <string>
#include <iostream>

using std::vector;
using std::list;
using std::queue;
using std::map;
using std::string;
using std::cin;
using std::cout;
using std::endl;

struct Query {
  int _size;
  int _source;
  int _destination;

  Query(int size, int source, int destination) : _size(size), _source(source), _destination(destination) { }
};

int main() {
  int M, N, P;
  map<string, int> warehouse_map;
  vector<list<int>> netlist;
  list<Query> queries;

  cin >> M >> N >> P;

  string code, code2;
  for (int i = 0; i < M; i++) {
    cin >> code;
    warehouse_map[code] = i;
  }

  netlist.resize(M);

  for (int i = 0; i < N; i++) {
    cin >> code >> code2;
    netlist[warehouse_map[code]].push_back(warehouse_map[code2]);
    netlist[warehouse_map[code2]].push_back(warehouse_map[code]);
  }

  int package;
  for (int i = 0; i < P; i++) {
    cin >> package >> code >> code2;
    queries.push_back(Query(package, warehouse_map[code], warehouse_map[code2]));
  }

  if (queries.empty())
    cout << -1 << endl;
  else {
    for (const Query &q : queries) {
      vector<int> distance(M);
      vector<bool> visited(M);
      queue<int> to_visit;

      to_visit.push(q._source);
      visited[q._source] = true;
      while (!to_visit.empty()) {
        int v = to_visit.front();
        to_visit.pop();
        for (int n : netlist[v]) {
          if (!visited[n]) {
            visited[n] = true;
            to_visit.push(n);
            distance[n] = distance[v] + 1;
          }
        }
      }

      if (visited[q._destination])
        cout << '$' << distance[q._destination] * q._size * 100 << endl;
      else
        cout << "NO SHIPMENT POSSIBLE" << endl;
    }
  }

  return 0;
}